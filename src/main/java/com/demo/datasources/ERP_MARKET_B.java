package com.demo.datasources;

import com.zaxxer.hikari.HikariDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
public class ERP_MARKET_B {

  private static final String prefix = "ERP_MARKET_B";

  private Environment env;

  public ERP_MARKET_B(Environment env) {
    this.env = env;
  }

  @Bean("DATASOURCE_ERP_MARKET_B")
  public DataSource dataSource() {

    DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
    dataSourceBuilder.driverClassName(getProperty("datasource.driver-class-name"));
    dataSourceBuilder.url(getProperty("datasource.url"));
    dataSourceBuilder.username(getProperty("datasource.username"));
    dataSourceBuilder.password(getProperty("datasource.password"));

    HikariDataSource dataSource = (HikariDataSource) dataSourceBuilder.build();
    dataSource.setPoolName(getProperty("datasource.hikari.name"));
    dataSource.setMinimumIdle(Integer.valueOf(getProperty("datasource.hikari.minimum-idle").trim()));
    dataSource.setMaximumPoolSize(Integer.valueOf(getProperty("datasource.hikari.maximum-pool-size").trim()));

    return dataSource;

  }

  @Bean("DATASOURCE_ERP_MARKET_B_TX_MANAGER")
  DataSourceTransactionManager dataSourceTransactionManager() {
    DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
    dataSourceTransactionManager.setDataSource(dataSource());
    return dataSourceTransactionManager;
  }

  @Bean("DATASOURCE_ERP_MARKET_B_TX_MANAGER_SQL_SESSION_FACTORY")
  public SqlSessionFactory sqlSessionFactory(
      @Qualifier("DATASOURCE_ERP_MARKET_B") DataSource dataSource,
      ApplicationContext applicationContext) throws Exception {

    SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
    sqlSessionFactoryBean.setDataSource(dataSource);

    sqlSessionFactoryBean.setConfigLocation(
        applicationContext.getResource("classpath:MyBatisConfig.xml"));
    sqlSessionFactoryBean.setMapperLocations(
        applicationContext.getResources("classpath*:mappers/ERP_MARKET_B/*.xml"));

    return sqlSessionFactoryBean.getObject();

  }

  @Bean("ERP_MARKET_B_SQL_SESSION")
  SqlSessionTemplate sqlSessionTemplate(
      @Qualifier("DATASOURCE_ERP_MARKET_B_TX_MANAGER_SQL_SESSION_FACTORY")
          SqlSessionFactory sqlSessionFactory) {
    return new SqlSessionTemplate(sqlSessionFactory);
  }

  private String getProperty(String key) {
    String prefixKey = String.format("%s.%s", prefix, key);
    return env.getProperty(prefixKey);
  }
}
