package com.demo.service;

import com.demo.model.구매내역;
import org.apache.camel.Body;
import org.apache.camel.Headers;
import org.apache.camel.ProducerTemplate;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Component
public class ERP_MARKET_B_SERVICE {

  private static final Logger logger = LoggerFactory.getLogger(ERP_MARKET_B_SERVICE.class);


  @Autowired
  ApplicationContext applicationContext;

  @Autowired
  ProducerTemplate producer;

  @Autowired
  @Qualifier("ERP_MARKET_B_SQL_SESSION")
  SqlSessionTemplate sqlSession;

  @Transactional("DATASOURCE_ERP_MARKET_B_TX_MANAGER")
  public Object process(@Body String body, @Headers Map<String, Object> headers) throws ClassNotFoundException {

    Object result = "OK";

    String s;

//    Gson gson = new Gson();
//    구매내역 pojo1 = gson.fromJson(body, Class.forName((String) headers.get("TableName")));
//    구매내역 pojo2 = gson.fromJson(body);

    s = producer.requestBody("direct:toJson", headers, String.class);

    logger.info("HEADERS : \n{}", s);
    logger.info("MESSAGE : \n{}", body);

    sqlSession = applicationContext.
        getBean("ERP_MARKET_B_SQL_SESSION", SqlSessionTemplate.class);

    List<구매내역> resultSet = sqlSession.<구매내역>selectList("mappers.구매내역Mapper.selectAll");

    s = producer.requestBody("direct:toJson", resultSet, String.class);

    logger.info("전체 구매 내역:\n{}", s);

    return result;
  }
}
