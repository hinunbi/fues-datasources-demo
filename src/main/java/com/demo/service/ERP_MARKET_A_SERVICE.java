package com.demo.service;

import com.demo.model.구매내역;
import org.apache.camel.Body;
import org.apache.camel.Headers;
import org.apache.camel.ProducerTemplate;
import org.apache.commons.lang3.RandomUtils;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Component
public class ERP_MARKET_A_SERVICE {

  private static final Logger logger = LoggerFactory.getLogger(ERP_MARKET_A_SERVICE.class);


  @Autowired
  ApplicationContext applicationContext;

  @Autowired
  ProducerTemplate producer;

  @Autowired
  @Qualifier("ERP_MARKET_A_SQL_SESSION")
  SqlSessionTemplate sqlSession;

  @Transactional("DATASOURCE_ERP_MARKET_A_TX_MANAGER")
  public Object process(@Body String body, @Headers Map<String, Object> headers) throws ClassNotFoundException {

    Object result = "OK";

    String s;

    s = producer.requestBody("direct:toJson", headers, String.class);

    logger.info("HEADERS : \n{}", s);
    logger.info("MESSAGE : \n{}", body);

    구매내역 in = producer.requestBody("direct:구매내역", body, 구매내역.class);
    in.set구매번호(Long.toString(RandomUtils.nextLong()));

    sqlSession = applicationContext.
        getBean("ERP_MARKET_A_SQL_SESSION", SqlSessionTemplate.class);

    sqlSession.insert("mappers.구매내역Mapper.insert", in);

    logger.info("등록 성공");

    return result;
  }
}
